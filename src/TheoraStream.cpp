#include "stdafx.h"

int main(int argc, char* argv[])
{
    int ret;
    int w = 1024;
    int h = 768;
    int stride = (w + 15) & ~15;
    
    std::ofstream f("out.ogv", std::ios::binary);

    ogg_stream_state stream;
    ogg_packet packet;
    ogg_page page;

    ogg_stream_init(&stream, rand());

    th_comment comment;
    th_comment_init(&comment);
    th_comment_add(&comment, "RANDOM=bla bla bla");

    th_info info;
    th_info_init(&info);
    info.frame_width = (w + 15) & ~15;
    info.frame_height = (h + 15) & ~15;
    info.pic_width = w;
    info.pic_height = h;
    info.fps_numerator = 24;
    info.fps_denominator = 1;
    info.quality = 23; // Quality [0,63] when using VBR
    info.keyframe_granule_shift = 5;

    th_ycbcr_buffer yuv;
    yuv[0].width = info.frame_width;
    yuv[0].height = info.frame_height;
    yuv[0].stride = yuv[0].width;
    yuv[0].data = (unsigned char*)malloc(yuv[0].stride * yuv[0].height);
    yuv[1].width = yuv[0].width / 2;
    yuv[1].height = yuv[0].height / 2;
    yuv[1].stride = yuv[1].width;
    yuv[1].data = (unsigned char*)malloc(yuv[1].stride * yuv[1].height);
    yuv[2].width = yuv[0].width / 2;
    yuv[2].height = yuv[0].height / 2;
    yuv[2].stride = yuv[2].width;
    yuv[2].data = (unsigned char*)malloc(yuv[2].stride * yuv[2].height);
    
    th_enc_ctx* ctx = th_encode_alloc(&info);

    // Push the header packets into the stream
    while ((ret = th_encode_flushheader(ctx, &comment, &packet)) != 0)
    {
        if (ret < 0) return -1;
        ret = ogg_stream_packetin(&stream, &packet);
    }
    
    // Get pages and write to file
    while ((ret = ogg_stream_pageout(&stream, &page)) != 0)
    {
        if (ret < 0) return -1;
        f.write((char*)page.header, page.header_len);
        f.write((char*)page.body, page.body_len);
    }
    
    // Get pages and write to file
    while ((ret = ogg_stream_flush(&stream, &page)) != 0)
    {
        if (ret < 0) return -1;
        f.write((char*)page.header, page.header_len);
        f.write((char*)page.body, page.body_len);
    }
    
    // Encode frame
    int tot_frames = 200;
    auto start = std::chrono::high_resolution_clock::now();
    double elasped = 0;
    int frames = 0;
    for (int i = 0; i < tot_frames; i++)
    {
        for (int y = 0; y < h; y++)
        {
            for (int x = 0; x < w; x++)
            {
                yuv[0].data[y*stride+x] = ((x+i)/10%2==0) && ((y+i)/10%3==0) ? (int)((float)x/w*255) : 0;
                yuv[1].data[y/2*stride+x/2] = (unsigned char)((std::sinf((float)y/h*100)*.5+.5)*100);
            }
        }
        
        ret = th_encode_ycbcr_in(ctx, yuv);
        ret = th_encode_packetout(ctx, i == tot_frames-1, &packet);
        ret = ogg_stream_packetin(&stream, &packet);
        // Get pages and write to file
        while ((ret = ogg_stream_pageout(&stream, &page)) != 0)
        {
            if (ret < 0) return -1;
            f.write((char*)page.header, page.header_len);
            f.write((char*)page.body, page.body_len);
        }
        frames++;
        auto stop = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double> diff = stop - start;
        start = stop;
        elasped += diff.count();
        if (elasped > 1.0)
        {
            elasped -= 1.0;
            printf("Encoded %3d/%d at %d fps\n", i, tot_frames, frames);
            frames = 0;
        }
//        printf("frame %3d encoded in %fms\r", i, diff.count());
    }

    // Get pages and write to file
    while ((ret = ogg_stream_flush(&stream, &page)) != 0)
    {
        if (ret < 0) return -1;
        f.write((char*)page.header, page.header_len);
        f.write((char*)page.body, page.body_len);
    }
    

    f.close();

    return 0;
}

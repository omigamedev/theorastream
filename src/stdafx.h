#if __WIN32
    #pragma once
    #include "targetver.h"
    #include <tchar.h>
#endif

#include <stdio.h>
#include <stdlib.h>

#include <fstream>
#include <chrono>
#include <cmath>

#include <theora/codec.h>
#include <theora/theora.h>
#include <theora/theoraenc.h>
